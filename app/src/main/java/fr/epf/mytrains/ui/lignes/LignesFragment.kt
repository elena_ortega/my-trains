package fr.epf.mytrains.ui.lignes

import android.os.Bundle
import android.view.*
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import fr.epf.mytrains.R

class LignesFragment : Fragment() {

    private lateinit var lignesViewModel: LignesViewModel

    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        lignesViewModel =
                ViewModelProviders.of(this).get(LignesViewModel::class.java)
        val root = inflater.inflate(R.layout.fragment_lignes, container, false)
        val textView: TextView = root.findViewById(R.id.text_lignes)
        lignesViewModel.text.observe(viewLifecycleOwner, Observer {
            textView.text = it
        })
        return root
    }
}
